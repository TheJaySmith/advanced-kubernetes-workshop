output "client_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.client_certificate}"
}