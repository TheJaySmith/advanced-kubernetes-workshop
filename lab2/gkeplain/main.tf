resource "google_container_cluster" "primary" {
  name                    = "${var.name}"
  zone                    = "${var.zone}"
  network                 = "${var.network}"
  min_master_version	  = "${var.min_master_version}"
  initial_node_count      = 3
  node_config {
    machine_type	  = "${var.machine_type}"
    image_type		  = "${var.image_type}"
  }
}
resource "null_resource" "cluster" {
   provisioner "local-exec" {
        command = "echo ${google_container_cluster.primary.project}  >> cluster-name.txt"
      }
   provisioner "local-exec" {
        command = "gcloud container clusters get-credentials ${google_container_cluster.primary.name} --zone ${google_container_cluster.primary.zone} --project ${google_container_cluster.primary.project}"
      }
   provisioner "local-exec" {
        command = "sleep 10; kubectx ${google_container_cluster.primary.name}=\"gke_\"${google_container_cluster.primary.project}\"_\"${google_container_cluster.primary.zone}\"_\"${google_container_cluster.primary.name}"
      }
   depends_on = ["google_container_cluster.primary"]
}
